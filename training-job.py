#!/usr/bin/env python

import requests
import os

import sagemaker
from sagemaker.estimator import Estimator
import boto3


session = sagemaker.Session(boto3.session.Session())

BUCKET_NAME = os.getenv('BUCKET_NAME')
PREFIX = os.getenv('PREFIX')

# Replace with your IAM role arn that has enough access (e.g. SageMakerFullAccess)
iam_role_name = 'SageMaker-Ali-CICD'

# Replace with your desired training instance
training_instance = 'ml.m5.large'

# Replace with your data s3 path
training_data_s3_uri = 's3://{}/{}/boston-housing-training.csv'.format(
    BUCKET_NAME, PREFIX)
validation_data_s3_uri = 's3://{}/{}/boston-housing-validation.csv'.format(
    BUCKET_NAME, PREFIX)

output_folder_s3_uri = 's3://{}/{}/output/'.format(BUCKET_NAME, PREFIX)
source_folder = 's3://{}/{}/source-folders'.format(BUCKET_NAME, PREFIX)
base_job_name = 'boston-housing-model'


# Define env variables
REGION = os.getenv('AWS_DEFAULT_REGION')
CI_COMMIT_SHORT_SHA = os.getenv('CI_COMMIT_SHORT_SHA')
CI_PROJECT_ID = os.getenv('CI_PROJECT_ID')
CI_PROJECT_NAME = os.getenv('CI_PROJECT_NAME')
CI_MERGE_REQUEST_IID = os.getenv('CI_MERGE_REQUEST_IID')
GITLAB_ACCESS_TOKEN = os.getenv('GITLAB_ACCESS_TOKEN')
ACCOUNT_ID = session.boto_session.client(
    'sts').get_caller_identity()['Account']


# Define estimator object
boston_estimator = Estimator(
    image_uri=f'{ACCOUNT_ID}.dkr.ecr.{REGION}.amazonaws.com/{CI_PROJECT_NAME}:latest',
    role=iam_role_name,
    instance_count=1,
    instance_type=training_instance,
    output_path=output_folder_s3_uri,
    code_location=source_folder,
    base_job_name='boston-housing-model',
    hyperparameters={'nestimators': 50},
    environment={"CI_MERGE_REQUEST_IID": CI_MERGE_REQUEST_IID,
                 "GITLAB_ACCESS_TOKEN": GITLAB_ACCESS_TOKEN,
                  "BUCKET_NAME": BUCKET_NAME,
                 "PREFIX": PREFIX,
                 "CI_COMMIT_SHORT_SHA": CI_COMMIT_SHORT_SHA,
                 "CI_PROJECT_ID": CI_PROJECT_ID,
                 "REGION": REGION,},

    tags=[{"Key": "email",
           "Value": "ali@datachef.co"}])

boston_estimator.fit({'training': training_data_s3_uri,
                      'validation': validation_data_s3_uri}, wait=False)


training_job_name = boston_estimator.latest_training_job.name
hyperparameters_dictionary = boston_estimator.hyperparameters()

message = (f"## Training Job Submission Report\n\n"
           f"Training Job name: '{training_job_name}'\n\n"
            "Model Artifacts Location:\n\n"
           f"'s3://{BUCKET_NAME}/{PREFIX}/output/{training_job_name}/output/model.tar.gz'\n\n"
           f"Model hyperparameters: {hyperparameters_dictionary}\n\n"
            "See the Logs in a few minute at: "
           f"[CloudWatch](https://{REGION}.console.aws.amazon.com/cloudwatch/home?region={REGION}#logStream:group=/aws/sagemaker/TrainingJobs;prefix={training_job_name})\n\n"
            "If you merge this pull request the resulting endpoint will be avaible this URL:\n\n"
           f"'https://runtime.sagemaker.{REGION}.amazonaws.com/endpoints/{training_job_name}/invocations'\n\n")


# POST the message to Merge Request Page
response = requests.post(f"https://gitlab.com/api/v4/projects/{CI_PROJECT_ID}/merge_requests/{CI_MERGE_REQUEST_IID}/notes",
                         headers={"PRIVATE-TOKEN": f"{GITLAB_ACCESS_TOKEN}"},
                         data={"body": message})
print('response status', response.status_code)
